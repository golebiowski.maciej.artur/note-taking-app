SHELL := /bin/bash -o pipefail -o errexit

INFRA_DIR = "infrastructure"
APP_DIR = "application"

only-deploy:
	$(MAKE) -C $(INFRA_DIR) only-deploy

only-destroy:
	$(MAKE) -C $(INFRA_DIR) only-destroy

build-app:
	$(MAKE) -C $(APP_DIR) build

test-app:
	$(MAKE) -C $(APP_DIR) test-lambda

deploy:
	$(MAKE) -C $(INFRA_DIR) deploy