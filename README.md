# Note taking app

## Overview

The goal to achive was to implement note taking app using serverless apprach on AWS cloud. 
### Architecture

To fulfill the criteria, I decided to use **APIGateway -> Lambda -> DynamoDB**, this architecture is auto-scaling and HA,<br> 
lest the bill grow too much, I added a cognito to let only authenticated traffic. <br><br>
![](./help/arch.png)

### Application

Application is created using Python 3.8 with FAST API freamwork and Dependency Injection pattern, <br>
to reduce coupling and increase cohesion in case of infrastructure architecture evloution.

### Infrastructure

To automate deployment on demand process, I decided to use Pulumi - IaC tool. <br>
This tool allows to provide IaC using moderng programming languages (JS/TS, Python, Go, C#). <br>
Pulumi under the hood used terraform providres but now it has their own. <br>
( Here I used terrform provider because pulumi_aws is quite new feature) <br>
I decleard whole infrastructure in \_\_main\_\_.py file to show Pulumi capabilities to package infrastrucute code. <br>

<br>
<br>
<br>

## Usage instruction

### Deployment

#### Prerequisites
* Linux system, if you don't have use Docker, Multipass etc
* Python3.8

#### Configuration and deployment

1. Create bucket for Pulumi remote state and rember it name
    1. Steps below base on AWS CLI default profile
    2. run `bash help/create_bucket.sh -b some-name -r eu-central-1`

2. Build:
    1. run `make build-app` in root dir, in application/lambda_notes/src should appear lib dir

3. Deploy
    1. export bucket name into terminal session `export BUCKET_NAME=some-name` 
    3. export region into terminal session `export AWS_DEFAULT_REGION=eu-central-1`
    2. run `make deploy` in root dir

#### Use
1. Get to know swagger from infrastructure/api/openapi.json
2. Get values from pulumi `cd infrastucture && pulumi stack output`
3. Create user with help/create_user.sh `# bash create_user.sh -u username -p password -c pool_id`
4. Call to api like in help/call.py
5. or just call api through AWS API Gateway console

to destroy infrastructure run `make only-destroy`