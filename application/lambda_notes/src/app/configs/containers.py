from dependency_injector import containers, providers
from app.services.dynamo_service import DynamoService
from app.services.date_service import DateService
import boto3


class Container(containers.DeclarativeContainer):

    config = providers.Configuration()

    session = providers.Resource(
        boto3.session.Session,
        aws_access_key_id=config.aws_access_key_id,
        aws_secret_access_key=config.aws_secret_access_key,
        aws_session_token=config.aws_session_token,
    )

    dynamodb = providers.Resource(
        session.provided.resource.call(),
        service_name='dynamodb',
    )

    service = providers.Factory(
        DynamoService,
        dynamodb_handler=dynamodb,
        table_name=config.table_name,
    )

    date_service = providers.Factory(
        DateService
    )

