from abc import ABC, abstractmethod
from typing import Optional
from app.contracts.models import Note, UpdateNote, CreateNote, GetAllNotes


class IDatabaseService(ABC):

    @abstractmethod
    def get_notes(slef, limit: int, offset: Optional[str] = None) -> GetAllNotes:
        ...

    @abstractmethod
    def get_note_by_id(self, note_id: str) -> Note:
        ...

    @abstractmethod
    def update_note(self, note_id: str, update_note: UpdateNote, update_date: str) -> bool:
        ...

    @abstractmethod
    def delete_note(self, note_id: str) -> bool:
        ...

    @abstractmethod
    def create_note(self, create_note: CreateNote, create_date: str) -> str:
        ...


class IDateService(ABC):

    @abstractmethod
    def get_date() -> str:
        ...
