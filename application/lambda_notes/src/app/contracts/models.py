from typing import List, Optional
from pydantic import BaseModel


class Note(BaseModel):
    id: str
    note: Optional[str]
    user: Optional[str]
    creationDate: Optional[str]
    lastModificationDate: Optional[str]


class GetAllNotes(BaseModel):
    offset: Optional[str]
    notes: List[Note]


class UpdateNote(BaseModel):
    note: str


class CreateNote(BaseModel):
    note: str
    user: str
