from typing import Optional
from functools import wraps
from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends, HTTPException

from app.configs.containers import Container
from app.contracts.interfaces import IDatabaseService, IDateService
from app.contracts.models import CreateNote, GetAllNotes, UpdateNote, Note
from app.services.dynamo_service import NoSuchItem, ItemAlreadyExists
router = APIRouter()

def http_codes_logic(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except NoSuchItem:
            raise HTTPException(status_code=404, detail="Note with given id not found")
        except ItemAlreadyExists:
            raise HTTPException(status_code=409, detail="Note id generation conflict, just repeat call")
    return wrapper

@router.get('/notes', response_model=GetAllNotes)
@inject
async def get_notes(
    offset: Optional[str] = None,
    limit: Optional[int] = None,
    default_limit: int = Depends(
        Provide[Container.config.default.limit.as_int()]),
    service: IDatabaseService = Depends(
        Provide[Container.service]
    )
):
    limit = limit or default_limit
    return service.get_notes(limit, offset)


@router.post("/notes", status_code=201)
@inject
@http_codes_logic
def create_note(
    note: CreateNote,
    service: IDatabaseService = Depends(
        Provide[Container.service]
    ),
    date_service: IDateService = Depends(
        Provide[Container.date_service]
    )
):
    date = date_service.get_date()
    return service.create_note(note, date)


@router.put("/notes/{note_id}", status_code=204)
@inject
def update_note(
    note_id: str,
    note: UpdateNote,
    service: IDatabaseService = Depends(
        Provide[Container.service]
    ),
    date_service: IDateService = Depends(
        Provide[Container.date_service]
    )
):
    date = date_service.get_date()
    return service.update_note(note_id, note, date)

@router.get('/notes/{note_id}', response_model=Note)
@inject
@http_codes_logic
def get_note(
    note_id: str,
    service: IDatabaseService = Depends(
        Provide[Container.service]
    )
):
    return service.get_note_by_id(note_id)

@router.delete('/notes/{note_id}', status_code=204)
@inject
@http_codes_logic
def delete_note(
    note_id: str,
    service: IDatabaseService = Depends(
        Provide[Container.service]
    )
):
    return service.delete_note(note_id)


