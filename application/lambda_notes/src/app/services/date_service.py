from app.contracts.interfaces import IDateService
import datetime

class DateService(IDateService):
    
    def __init__(self):
        dt = datetime.datetime.today()
        self.now = f"{dt.year}-{dt.month}-{dt.day}"
    
    def get_date(self)->str:
        return self.now
