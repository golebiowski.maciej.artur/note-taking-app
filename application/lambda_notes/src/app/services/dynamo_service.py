import uuid
from functools import wraps
from typing import Optional

from app.contracts.interfaces import IDatabaseService
from app.contracts.models import CreateNote, GetAllNotes, Note, UpdateNote
from botocore.exceptions import ClientError


class NoSuchItem(Exception):
    pass


class ItemAlreadyExists(Exception):
    pass


def dynamo_error_logic(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ClientError as client_error:
            if client_error.response["Error"]["Code"] == "ConditionalCheckFailedException":
                raise NoSuchItem
            else:
                raise client_error
        except KeyError as key_error:
            if key_error.args[0] == "Item":
                raise NoSuchItem
            else:
                raise key_error

    return wrapper


class DynamoService(IDatabaseService):
    KEY = "id"
    NOTE_KEY = "note"
    CREATE_DATE = "creationDate"
    LAST_MODIFICATION_DATE = "lastModificationDate"

    @staticmethod
    def create_id():
        return str(uuid.uuid4())

    def __init__(self, dynamodb_handler, table_name):
        self.table = dynamodb_handler.Table(table_name)

    def get_notes(self, limit: int, offset: Optional[str] = None) -> GetAllNotes:
        params = {"Limit": limit}
        if offset is not None:
            params["ExclusiveStartKey"] = {self.KEY:offset}

        result = self.table.scan(**params)
        notes = list(map(Note.parse_obj, result.get("Items", [])))

        offset = result.get("LastEvaluatedKey", {}).get(self.KEY, None)
        return GetAllNotes(offset=offset, notes=notes)

    @dynamo_error_logic
    def get_note_by_id(self, note_id: str) -> Note:
        return Note.parse_obj(self.table.get_item(Key={self.KEY: note_id})["Item"])

    @dynamo_error_logic
    def update_note(self, note_id: str, update_note: UpdateNote, update_date: str) -> bool:
        return bool(
            self.table.update_item(
                Key={self.KEY: note_id},
                UpdateExpression=f"set {self.NOTE_KEY}=:n, {self.LAST_MODIFICATION_DATE}=:l",
                ConditionExpression=f"attribute_exists({self.KEY})",
                ExpressionAttributeValues={
                    ":n": update_note.note,
                    ":l": update_date
                },
                ReturnValues="UPDATED_NEW",
            )
        )

    @dynamo_error_logic
    def delete_note(self, note_id: str) -> bool:
        self.table.delete_item(
            Key={self.KEY: note_id},
            ConditionExpression=f"attribute_exists({self.KEY})",
        )
        return True

    def create_note(self, create_note: CreateNote, create_date: str) -> str:
        note_id = self.create_id()
        item = create_note.dict()
        item[self.KEY] = note_id
        item[self.CREATE_DATE] = create_date
        try:
            self.table.put_item(
                Item=item, ConditionExpression=f"attribute_not_exists({self.KEY})")
        except ClientError as client_error:
            if client_error.response["Error"]["Code"] == "ConditionalCheckFailedException":
                raise ItemAlreadyExists
            else:
                raise client_error
        return note_id
