from fastapi import FastAPI
from mangum import Mangum
from app.configs.containers import Container
from app import notes_router


def create_app():
    container = Container()

    container.config.default.limit.from_env('LIMIT')
    container.config.table_name.from_env('TABLE_NAME')
    container.config.aws_access_key_id.from_env('AWS_ACCESS_KEY_ID')
    container.config.aws_secret_access_key.from_env('AWS_SECRET_ACCESS_KEY')
    container.config.aws_session_token.from_env('AWS_SESSION_TOKEN')

    container.wire(modules=[notes_router])
    # container.init_resources()
    
    app = FastAPI()
    app.container = container
    app.include_router(notes_router.router)
    handler = Mangum(app)
    return handler, app


handler, app = create_app()
