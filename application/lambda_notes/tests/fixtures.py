import os 
import boto3
import pytest
from moto import mock_dynamodb2
from lambda_notes.tests.mock import mock_note_db_object
from lambda_notes.src.app.services.dynamo_service import DynamoService

@pytest.fixture()
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"

@pytest.fixture()
def dynamo_service(aws_credentials):
    table_name = "test"
    with mock_dynamodb2():
        res = boto3.resource("dynamodb", "us-east-2")
        res.create_table(
            TableName=table_name,
            KeySchema=[
                {"AttributeName": "id", "KeyType": "HASH"},
            ],
            AttributeDefinitions=[
                {"AttributeName": "id", "AttributeType": "S"},
            ],
            ProvisionedThroughput={
                "ReadCapacityUnits": 1,
                "WriteCapacityUnits": 1,
            },
        )
        table = res.Table(table_name)
        table.put_item(Item=mock_note_db_object())
        table.put_item(Item=mock_note_db_object("123"))
        yield DynamoService(res, table_name)
