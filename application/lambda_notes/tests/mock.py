import uuid

def mock_note_db_object(note_id=None):
    return {
        "id": note_id or str(uuid.uuid4()),
        "note": "note",
        "user": "email@email.com",
        "creationDate": "2015-07-07",
        "lastModificationDate": "2015-07-07",
    }
