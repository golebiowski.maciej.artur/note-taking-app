from pydantic.tools import T
from lambda_notes.src.app.services.dynamo_service import DynamoService, NoSuchItem
from lambda_notes.src.app.contracts.models import CreateNote, UpdateNote
from lambda_notes.tests.fixtures import dynamo_service, aws_credentials
import pytest


def test_create_id():
    assert isinstance(DynamoService.create_id(), str)


def test_delete_note(dynamo_service: DynamoService):
    assert dynamo_service.delete_note("123") == True

    with pytest.raises(NoSuchItem):
        dynamo_service.delete_note("123")


def test_create_note(dynamo_service: DynamoService):
    assert isinstance(dynamo_service.create_note(CreateNote(
        note="note", user="mail@mail.com"), "2015-07-07"), str)


def test_get_notes(dynamo_service: DynamoService):
    res = dynamo_service.get_notes(100)
    assert len(res.notes) == 2
    assert res.offset is None

    res_offset = dynamo_service.get_notes(limit=1)
    assert len(res_offset.notes) == 1
    assert isinstance(res_offset.offset, str)


def test_get_note_by_id(dynamo_service: DynamoService):
    res_by_id = dynamo_service.get_note_by_id("123")
    assert res_by_id.id == "123"

    with pytest.raises(NoSuchItem):
        dynamo_service.get_note_by_id("not123")


def test_update_note(dynamo_service: DynamoService):
    note = UpdateNote(note="-")
    assert dynamo_service.update_note("123", note, "2015-07-07") is True
    with pytest.raises(NoSuchItem):
        dynamo_service.update_note("not123", note, "2015-07-07")
