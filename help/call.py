#!/usr/bin/python3
import json
import sys
import os
import requests


def payload(user, password, client_id):

    return json.dumps({
        "AuthParameters": {
            "USERNAME": user,
            "PASSWORD": password
        },
        "AuthFlow": "USER_PASSWORD_AUTH",
        "ClientId": client_id
    })


if __name__ == "__main__":

    headers = {
        "X-Amz-Target": "AWSCognitoIdentityProviderService.InitiateAuth",
        "Content-Type": "application/x-amz-json-1.1"
        }
    region = os.environ.get("AWS_DEFAULT_REGION")
    idp_url = f"https://cognito-idp.{region}.amazonaws.com/"

    if len(sys.argv) == 5:
        user = sys.argv[1]
        password = sys.argv[2]
        client_id = sys.argv[3]
        url = sys.argv[4]

        data = payload(user=user, password=password, client_id=client_id)
        print(data)
    else:
        print("Give username, password, pool api url id in this order")
        exit(1)

    resp = requests.post(
        idp_url,
        data=data,
        headers=headers
    ).json().get("AuthenticationResult")

    i_tkn = resp["IdToken"]
    
    notes = requests.get(f"{url}/notes", headers={"Authorization": i_tkn} ).json()

    print(notes)
