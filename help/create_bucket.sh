
#!/bin/bash


while getopts b:r: flag
do
    case "${flag}" in
        b) bucket=${OPTARG};;
        r) region=${OPTARG};;
    esac
done

check_arg(){
    if [ -z "$1" ]
    then
        echo "No argument $2"
        exit
    else
        echo "Argument $1"
    fi
}

check_arg "$bucket" "bucket, provide -b, it should be unique"
check_arg "$region" "region, provide -r"

if aws s3 ls "$bucket" 2>&1 | grep -q 'NoSuchBucket'; then
    echo $(aws s3api create-bucket --bucket $bucket --region $region --create-bucket-configuration LocationConstraint=$region)
else
    echo "bucket already exists"
fi