#!/bin/bash

# bash create_user.sh -u username -p password -c pool_id

while getopts u:p:c: flag
do
    case "${flag}" in
        u) username=${OPTARG};;
        p) password=${OPTARG};;
        c) pool_id=${OPTARG};;
    esac
done

check_arg(){
    if [ -z "$1" ]
    then
        echo "No argument $2"
        exit
    else
        echo "Argument $1"
    fi
}

check_arg "$username" "username, provide -u in email format"
check_arg "$password" "password, provide -p uppercase, lowercase, number and special character"
check_arg "$pool_id" "pool_id, provide -c"

aws cognito-idp admin-create-user --user-pool-id $pool_id --username $username

aws cognito-idp admin-set-user-password --user-pool-id $pool_id --username $username --password $password --permanent