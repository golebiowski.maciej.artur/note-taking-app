import pulumi

from api.api_gateway import PATHS_METHODS, create_api
from api.parametrize_openapi import APISetup
from cognito.user_pool import create_user_pool
from configs import DynamoTableConfig, LambdaConfig
from dynamo.table import create_table
from lambda_.lambda_ import create_lambda_for_dynamo_service
from lambda_.lambda_iam import lambda_role
from props import ACCOUNT, APP_NAME, REGION

table = create_table(DynamoTableConfig(APP_NAME))
role = lambda_role(APP_NAME, table, REGION, ACCOUNT)
lambda_ = create_lambda_for_dynamo_service(
    LambdaConfig(APP_NAME, "main.handler",
                 "./../application/lambda_notes/src/"),
    table,
    role
)
pool, client = create_user_pool(APP_NAME)

api_setup = APISetup(PATHS_METHODS, APP_NAME, lambda_, pool)

deployment = create_api(api_setup)

pulumi.export("api-url", deployment.invoke_url)
pulumi.export("pool-id", pool.id)
pulumi.export("client-id", client.id)
