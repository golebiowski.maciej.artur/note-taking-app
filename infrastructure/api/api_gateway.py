import json

import pulumi
import pulumi_aws as aws
from api.parametrize_openapi import HTTPMethod
from api.parametrize_openapi import APISetup

PATHS_METHODS = [
    ("/notes", HTTPMethod.GET),
    ("/notes", HTTPMethod.POST),
    ("/notes/{noteId}", HTTPMethod.GET),
    ("/notes/{noteId}", HTTPMethod.PUT),
    ("/notes/{noteId}", HTTPMethod.DELETE),
]


def create_api(api: APISetup
)->aws.apigateway.Deployment:

    body = api.get_openapi()
    
    rest_api = aws.apigateway.RestApi(
        api.api_name,
        policy=json.dumps(
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Principal": "*",
                        "Action": "execute-api:Invoke",
                        "Resource": ["*"],
                    }
                ],
            }
        ),
        endpoint_configuration={"types": "REGIONAL"},
        body=body,
        opts=pulumi.ResourceOptions(parent=api.lambda_service_all_endpoints, depends_on=[api.lambda_service_all_endpoints]),
    )

    aws.lambda_.Permission(
        api.api_name,
        action="lambda:InvokeFunction",
        function=api.lambda_service_all_endpoints.name,
        principal="apigateway.amazonaws.com",
        source_arn=rest_api.execution_arn.apply(lambda arn: f"{arn}/*/*/*"),
        opts=pulumi.ResourceOptions(parent=api.lambda_service_all_endpoints, depends_on=[api.lambda_service_all_endpoints]),
    )

    return aws.apigateway.Deployment(
        api.api_name,
        rest_api=rest_api.id,
        stage_name="dev",
        triggers={"swagger": body},
        opts=pulumi.ResourceOptions(parent=rest_api),
    )
