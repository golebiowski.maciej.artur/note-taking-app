import json
from enum import Enum
from typing import Optional

import pulumi
from pulumi_aws.cognito import UserPool
from pulumi_aws.lambda_ import Function


class HTTPMethod(Enum):
    GET = "get"
    POST = "post"
    DELETE = "delete"
    PUT = "put"


class APISetup:

    DAFAULT_RESPONSES_PATH = "./api/parametrizations/default_responses.json"
    OPTIONS_PATH = "./api/parametrizations/options.json"
    COGNITO_AUTHORIZER_PATH = "./api/parametrizations/cognito_authorizer.json"
    OPENAPI_PATH = "./api/openapi.json"

    @staticmethod
    def __read_json(path: str):
        with open(path) as json_file:
            return json.load(json_file)

    @staticmethod
    def __set_security(openapi, path: str, method: HTTPMethod, user_pool_name):
        openapi["paths"][path][method.value]["security"] = [
            {user_pool_name: []}]

    @staticmethod
    def __set_security_schemes(openapi,user_pool_name, user_pool_arn):
        openapi["components"]["securitySchemes"] =  {
            user_pool_name: {
                "type": "apiKey",
                "name": "Authorization",
                "in": "header",
                "x-amazon-apigateway-authtype": "cognito_user_pools",
                "x-amazon-apigateway-authorizer": {
                    "type": "cognito_user_pools",
                    "providerARNs": [
                        user_pool_arn
                    ]
                }
            }
        }

    @staticmethod
    def __set_apigw_integration(openapi, path: str, method: HTTPMethod, lambda_uri: str):
        openapi["paths"][path][method.value]["x-amazon-apigateway-integration"] = {
            "uri": lambda_uri,
            "passthroughBehavior": "when_no_match",
            "httpMethod": "POST",
            "type": "aws_proxy",
        }

    def __init__(self, paths, api_name: str, lambda_: Function,  user_pool: Optional[UserPool] = None):
        self.paths = paths
        self.api_name = api_name
        self.__user_pool = user_pool
        self.lambda_service_all_endpoints = lambda_
        self.__openapi = APISetup.__read_json(APISetup.OPENAPI_PATH)

    def get_openapi(self):
        params = [
            self.lambda_service_all_endpoints.invoke_arn,
        ]
        if self.__user_pool is not None:
            params.append(self.__user_pool.name)
            params.append(self.__user_pool.arn)
        
        return pulumi.Output.all(
            *params
        ).apply(lambda outputs: self.__parametrized_openapi(
            *outputs
        ))

    def __parametrized_openapi(self, lambda_invoke_arn, user_pool_name=None, user_pool_arn=None):
        parametrized_openapi = self.__openapi.copy()
        parametrized_openapi["info"]["title"] = self.api_name
        for path, method in self.paths:
            self.__set_apigw_integration(
                parametrized_openapi, path, method, lambda_invoke_arn
            )
            self.__set_options_cors(parametrized_openapi, path)
            if self.__user_pool is not None:
                self.__set_security(parametrized_openapi, path, method, user_pool_name)

        if self.__user_pool is not None:
            self.__set_security_schemes(
                parametrized_openapi, user_pool_name, user_pool_arn
            )

        self.__set_default_gateway_responses(parametrized_openapi)
        return json.dumps(parametrized_openapi)

    def __set_default_gateway_responses(self, openapi):
        openapi["x-amazon-apigateway-gateway-responses"] = self.__read_json(
            self.DAFAULT_RESPONSES_PATH)

    def __set_cognito_authorizer(self, openapi):
        openapi["components"]["securitySchemes"] = self.__read_json(
            self.COGNITO_AUTHORIZER_PATH)

    def __set_options_cors(self, openapi,  path: str):
        openapi["paths"][path]["options"] = self.__read_json(
            self.OPTIONS_PATH)
