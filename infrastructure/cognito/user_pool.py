import pulumi_aws as aws


def create_user_pool(pool_name,):
    pool = aws.cognito.UserPool(
        pool_name
    )
    client = aws.cognito.UserPoolClient(
        "client",
        user_pool_id=pool.id,
        explicit_auth_flows=["ALLOW_REFRESH_TOKEN_AUTH",
                             "ALLOW_USER_PASSWORD_AUTH"]
    )
    return pool, client
