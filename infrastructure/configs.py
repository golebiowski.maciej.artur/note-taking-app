class ResourceConfig:
    def __init__(self, resource_name: str):
        self.resource_name = resource_name


class LambdaConfig(ResourceConfig):
    def __init__(self, resource_name: str, handler: str, source_code: str):
        self.resource_name = resource_name
        self.handler = handler
        self.source_code = source_code


class ApiConfig(ResourceConfig):
    ...


class DynamoTableConfig(ResourceConfig):
    def __init__(self, resource_name: str, hash_key="id", read_capacity=10, write_capacity=10):
        self.resource_name = resource_name
        self.hash_key = hash_key
        self.write_capacity = write_capacity
        self.read_capacity = read_capacity
