import pulumi_aws as aws

from configs import DynamoTableConfig


def create_table(table_config: DynamoTableConfig):
    return aws.dynamodb.Table(
        resource_name=table_config.resource_name,
        hash_key=table_config.hash_key,
        attributes=[
            aws.dynamodb.TableAttributeArgs(
                name=table_config.hash_key,
                type="S",
            )
        ],
        ttl=aws.dynamodb.TableTtlArgs(
            attribute_name="ttl",
            enabled=True,
        ),
        read_capacity=table_config.read_capacity,
        write_capacity=table_config.write_capacity,
    )
