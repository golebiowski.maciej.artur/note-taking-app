import pulumi
import pulumi_aws as aws
from lambda_.lambda_iam import lambda_role
from configs import LambdaConfig


def create_envs(table_name):
    return {"PYTHONPATH": "/var/task/lib", "TABLE_NAME": table_name, "LIMIT": 100}


def create_lambda_for_dynamo_service(
    lambda_config: LambdaConfig, table: aws.dynamodb.Table, role: aws.iam.Role
) -> aws.lambda_.Function:

    return aws.lambda_.Function(
        lambda_config.resource_name,
        handler=lambda_config.handler,
        code=pulumi.asset.FileArchive(lambda_config.source_code),
        role=role.arn,
        runtime="python3.8",
        memory_size=1024,
        timeout=5,
        environment={"variables": pulumi.Output.all(table.name).apply(lambda x: create_envs(*x))},
    )
