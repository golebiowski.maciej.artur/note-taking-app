import pulumi
import pulumi_aws as aws
from policies.dynamic_lambda_policies import create_policy_table


def lambda_role(lambda_name: str, table: aws.dynamodb.Table, region, account_id) -> aws.iam.Role:
    role = aws.iam.Role(lambda_name, assume_role_policy=(
        lambda path: open(path).read())("./policies/lambda_assume.json"))

    aws.iam.RolePolicy(
        lambda_name,
        role=role.id,
        policy=pulumi.Output.all(table.name).apply(
            lambda x: create_policy_table(region, account_id,*x)
        ),
        opts=pulumi.ResourceOptions(parent=role),
    )

    return role
