import json


def create_policy_table(region, account, table_name) -> str:
    return json.dumps(
        {
            "Version": "2012-10-17",
            "Statement": [
                {"Effect": "Allow", "Action": "logs:CreateLogGroup", "Resource": f"arn:aws:logs:{region}:{account}:*"},
                {
                    "Effect": "Allow",
                    "Action": ["logs:CreateLogStream", "logs:PutLogEvents"],
                    "Resource": [f"arn:aws:logs:{region}:{account}:log-group:/aws/lambda/*:*"],
                },
                {
                    "Effect": "Allow",
                    "Action": [
                        "dynamodb:BatchGetItem",
                        "dynamodb:GetItem",
                        "dynamodb:Query",
                        "dynamodb:Scan",
                        "dynamodb:BatchWriteItem",
                        "dynamodb:PutItem",
                        "dynamodb:UpdateItem",
                        "dynamodb:DeleteItem",
                    ],
                    "Resource": f"arn:aws:dynamodb:{region}:{account}:table/{table_name}",
                },
            ],
        }
    )
