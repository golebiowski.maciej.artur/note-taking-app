import pulumi_aws as aws

APP_NAME = "note-taking-app"
ACCOUNT = aws.get_caller_identity().account_id
REGION = aws.get_region().name
