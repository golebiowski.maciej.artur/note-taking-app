#!/bin/bash

# SETUP PULUMI
curl -fsSL https://get.pulumi.com | sh -s -- --version 3.14.0

export PATH=$HOME/.pulumi/bin:$PATH
        
export PULUMI_CONFIG_PASSPHRASE=''

pulumi login s3://$BUCKET_NAME

# SELECT OR CREATE STACK ( STATE SPACE IN BUCKET )
pulumi stack select note-taking-app --create

# SETUP PULUMI CONFIG
pulumi config set aws:region $AWS_DEFAULT_REGION

# INSTALL PULUMI PACKAGES 
pip install -r requirements.txt

# DEPLOY
pulumi up --yes --skip-preview
